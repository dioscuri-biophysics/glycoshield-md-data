# GlycoSHIELD-MD-Data

Copy of GROMACS trajectories and reference structures of glycans
used by the GlycoSHIELD pipeline, originally published via Zenodo,
e.g.

https://doi.org/10.5281/zenodo.8422323

The folder structure is directly derived from the DOI.

Zenodo datasets can be easily updated using the tool 'zenodo_get'
from https://gitlab.com/dvolgyes/zenodo_get

This repository and the data are used by
https://gitlab.mpcdf.mpg.de/dioscuri-biophysics/glycoshield-md
